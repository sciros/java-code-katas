package com.careworkstech.exercise;

public class AnagramTrasformer {
    /**
     * Returns the input string with two characters flipped: the
     * character at leftIndex and the character at leftIndex+1.
     * Example: flip("hello",4) returns "helol"
     *
     * This means that leftIndex cannot exceed the string's
     * length minus one, e.g., if the string is 6 characters
     * long then leftIndex cannot exceed 4.
     *
     * @param input the input string to operate on
     * @param leftIndex the index of the left character in the flip
     * @return input string with characters in positions leftIndex and
     * leftIndex+1 flipped.
     */
    String flip(String input, Integer leftIndex) {
        return null; //FIXME implement this
    }

    /**
     * Returns the number of flip operations that it took to transform the first
     * string into the target string.
     * <p>
     * It should also print every intermediate string.
     * <p>
     * Note: no other operation except flip() may modify the starting string.
     * All other methods invoked on it may not modify it.
     * <p>
     * Both strings are anagrams of each other.
     * An anagram of a word is a word made by rearranging the letters.
     * Examples: rat, art; silent, listen
     *
     * @param start  a string to perform flip operations on
     * @param target the string to transform the starting string into
     * @return the number of flip operations it took to transform start
     * into target.
     */
    Integer flipsToTransform(String start, String target) {
        return null; //FIXME implement this
    }
}
