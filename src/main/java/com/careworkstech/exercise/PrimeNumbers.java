package com.careworkstech.exercise;

import java.util.List;

class PrimeNumbers {
    /**
     * Returns a list of all the prime numbers between 1 and provided integer.
     * A number is prime if it is only divisible by 1 and by itself. Composite otherwise.
     * 2 is the first prime. 1 is neither prime nor composite.
     *
     * @param max the maximum number of the range to search for primes
     * @return a List of Integers containing only all the primes between 1 and max
     */
    List<Integer> primes(Integer max) {
        return null; //FIXME implement this
    }
}
