package com.careworkstech.exercise;

class RomanNumerals {
    /**
     * Converts input in decimal form to a Roman Numeral string.
     * Examples: 3 to "III", 19 to "XIX", 26 to "XXVI"
     *
     * Conversion help:
     *   M is 1000, D is 500, C is 100, L is 50, X is 10, V is 5, I is 1
     *   CM is 900, CD is 400, XC is 90, XL is 40, IX is 9, IV is 4
     *
     * @param decimal an Integer representing a decimal value
     * @return the Roman Numeral equivalent of the input as a String
     */
    String toRoman (Integer decimal) {
        return null; //FIXME implement this
    }

    /**
     * Converts input in Roman Numeral form to an Integer (decimal form).
     * Examples: "XCVIII" to 98, "XXIV" to 24, "II" to 2
     *
     * @param roman a String representing a Roman Numeral
     * @return the Integer equivalent of the input
     */
    Integer toDecimal (String roman) {
        return null; //FIXME implement this
    }
}
