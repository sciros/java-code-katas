package com.careworkstech.exercise;

import java.util.List;

class StringSorter {
    /**
     * Sorts list of strings according to their "cost."
     * A string's "cost" is the sum of the "cost" of each letter.
     * The "cost" of each letter is its place in the alphabet (e.g. a = 1, z = 26)
     *
     * @param strings A list of strings (lower-case alpha chars only)
     * @return A sorted list of strings, sorted by each string's "cost" ascending
     */

    //Constant for the letters and their cost

    List<String> sortStringsByCost(List<String> strings) {
        return null; //FIXME implement this
    }
}
