package com.careworkstech.exercise;

public class Fizzbuzz {
    /**
     * Fizzbuzz iterates over the numbers 1-100
     * For each number:
     *    print the number itself, with the following exceptions:
     *    print 'fizz' if the number is divisible by 3 (but not 5)
     *    print 'buzz' if the number is divisible by 5 (but not 3)
     *    print 'fizzbuzz' if the number is divisible by both 3 and 5
     */
    public void fizzbuzz() {
    }
}
