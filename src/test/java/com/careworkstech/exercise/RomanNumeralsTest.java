package com.careworkstech.exercise;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

public class RomanNumeralsTest {
    private RomanNumerals romanNumerals;

    @Before
    public void setUp() {
        romanNumerals = new RomanNumerals();
    }

    @Test
    public void toRomanShouldReturnIGiven1() {
        assertThat(romanNumerals.toRoman(1)).isEqualToIgnoringCase("I");
    }

    @Test
    public void toRomanShouldReturnIIGiven2() {
        assertThat(romanNumerals.toRoman(2)).isEqualToIgnoringCase("II");
    }

    @Test
    public void toRomanShouldReturnIVGiven4() {
        assertThat(romanNumerals.toRoman(4)).isEqualToIgnoringCase("IV");
    }

    @Test
    public void toRomanShouldReturnVIIIGiven8() {
        assertThat(romanNumerals.toRoman(8)).isEqualToIgnoringCase("VIII");
    }

    @Test
    public void toRomanShouldReturnIXGiven9() {
        assertThat(romanNumerals.toRoman(9)).isEqualToIgnoringCase("IX");
    }

    @Test
    public void toRomanShouldReturnXIVGiven14() {
        assertThat(romanNumerals.toRoman(14)).isEqualToIgnoringCase("XIV");
    }

    @Test
    public void toRomanShouldReturnMCMXCVIIIGiven1998() {
        assertThat(romanNumerals.toRoman(1998)).isEqualToIgnoringCase("MCMXCVIII");
    }

    @Test
    public void toDecimalShouldReturn1GivenI() {
        assertThat(romanNumerals.toDecimal("I")).isEqualTo(1);
    }

    @Test
    public void toDecimalShouldReturn2GivenII() {
        assertThat(romanNumerals.toDecimal("II")).isEqualTo(2);
    }

    @Test
    public void toDecimalShouldReturn4GivenIV() {
        assertThat(romanNumerals.toDecimal("IV")).isEqualTo(4);
    }

    @Test
    public void toDecimalShouldReturn10GivenX() {
        assertThat(romanNumerals.toDecimal("X")).isEqualTo(10);
    }

    @Test
    public void toDecimalShouldReturn19GivenXIX() {
        assertThat(romanNumerals.toDecimal("XIX")).isEqualTo(19);
    }

    @Test
    public void toDecimalShouldReturn1998GivenMCMXCVIII() {
        assertThat(romanNumerals.toDecimal("MCMXCVIII")).isEqualTo(1998);
    }
}
