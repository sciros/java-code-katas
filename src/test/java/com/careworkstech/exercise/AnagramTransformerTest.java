package com.careworkstech.exercise;

import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;


public class AnagramTransformerTest {
    private AnagramTrasformer anagramTransformer;

    @Before
    public void setUp() {
        anagramTransformer = new AnagramTrasformer();
    }

    @Test
    public void flipShouldFlipTheCharacterInInputStringSpecifiedByLeftPosition() {
        assertThat(anagramTransformer.flip("hello",0)).isEqualToIgnoringCase("ehllo");
        assertThat(anagramTransformer.flip("hello",1)).isEqualToIgnoringCase("hlelo");
        assertThat(anagramTransformer.flip("hello",3)).isEqualToIgnoringCase("helol");
        assertThat(anagramTransformer.flip("howdy", 2)).isEqualToIgnoringCase("hodwy");
    }

    @Test
    public void flipsToTransformShouldReturnNumberOfFlipOperationsToTransformStartStringIntoTargetString() {
        assertThat(anagramTransformer.flipsToTransform("married","admirer")).isEqualTo(9);
        assertThat(anagramTransformer.flipsToTransform("listen","silent")).isEqualTo(5);
        assertThat(anagramTransformer.flipsToTransform("deductions","discounted")).isEqualTo(26);
    }
}


