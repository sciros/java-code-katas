package com.careworkstech.exercise;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

public class PrimeNumbersTest {
    private PrimeNumbers primeNumbers;

    @Before
    public void setUp() {
        primeNumbers = new PrimeNumbers();
    }

    @Test
    public void primesShouldFindAllThePrimeNumbersBetween1And10() {
        assertThat(primeNumbers.primes(10)).containsExactly(2, 3, 5, 7);
    }

    @Test
    public void primesShouldFindAllThePrimeNumbersBetween1And50() {
        assertThat(primeNumbers.primes(50)).containsExactly(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47);
    }

    @Test
    public void primesShouldFindAllThePrimeNumbersBetween1And100() {
        assertThat(primeNumbers.primes(100)).containsExactly(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97);
    }

    @Test
    public void primesShouldFindAllThePrimeNumbersBetween1And200() {
        assertThat(primeNumbers.primes(200)).containsExactly(2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47, 53, 59, 61, 67, 71, 73, 79, 83, 89, 97, 101, 103, 107, 109, 113, 127, 131, 137, 139, 149, 151, 157, 163, 167, 173, 179, 181, 191, 193, 197, 199);
    }
}
