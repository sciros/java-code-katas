package com.careworkstech.exercise;

import org.junit.BeforeClass;
import org.junit.Test;

import static org.assertj.core.api.Assertions.*;

public class StringConverterTest {

    private static StringConverter stringConverter;

    @BeforeClass
    public static void init() {
        stringConverter = new StringConverter();
    }

    @Test
    public final void convertLettersToNumbersShouldReturn326Givencz(){
        assertThat(stringConverter.convertLettersToNumbers("cz")).isEqualTo(326);
    }

    @Test
    public final void convertLettersToNumbersShouldReturn2468GivenABCD(){
        assertThat(stringConverter.convertLettersToNumbers("ABCD")).isEqualTo(2468);
    }

    @Test
    public final void convertLettersToNumbersShouldReturn14340GivenaBcT(){
        assertThat(stringConverter.convertLettersToNumbers("aBcT")).isEqualTo(14340);
    }

    @Test
    public final void converLettersToNumbersShouldReturnNegative1GivenNonAlphaCharacters(){
        assertThat(stringConverter.convertLettersToNumbers("Az,B$y")).isEqualTo(-1);
    }

    @Test
    public final void convertLettersToNumbersShouldReturnNegative1GivenEmptyString(){
        assertThat(stringConverter.convertLettersToNumbers("")).isEqualTo(-1);
    }

    @Test
    public final void convertLettersToNumbersShouldReturnNegative1GivenNull(){
        assertThat(stringConverter.convertLettersToNumbers(null)).isEqualTo(-1);
    }

    @Test
    public void convertLettersToNumbersShouldReturnNegative1GivenTooLongString(){
        assertThat(stringConverter.convertLettersToNumbers("TTTTTTTTTTT")).isEqualTo(-1);
    }
}
