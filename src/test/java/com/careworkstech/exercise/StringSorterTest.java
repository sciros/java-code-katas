package com.careworkstech.exercise;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.assertj.core.api.Assertions.*;

public class StringSorterTest {
    private StringSorter stringSorter;

    @Before
    public void setUp () {
        stringSorter = new StringSorter();
    }

    @Test
    public void sortStringsByCostShouldSortStringsAccordingToCost () {
        assertThat(stringSorter.sortStringsByCost(new ArrayList<>(Arrays.asList("tem","cab","exz")))).containsExactly("cab","tem","exz");
        assertThat(stringSorter.sortStringsByCost(new ArrayList<>(Arrays.asList("az","bc","hi","jk")))).containsExactly("bc","hi","jk","az");
    }

    @Test
    public void sortStringsByCostShouldNotDropDifferentStringsOfEqualCost () {
        assertThat(stringSorter.sortStringsByCost(new ArrayList<>(Arrays.asList("az","bc","cb","hi")))).containsExactlyInAnyOrder("az","bc","cb","hi");
        assertThat(stringSorter.sortStringsByCost(new ArrayList<>(Arrays.asList("az","bc","cb","hi")))).endsWith("az");
    }

    @Test
    public void sortStringsByCostShoudldTreatEmptyStringAsHavingCostZero () {
        assertThat(stringSorter.sortStringsByCost(new ArrayList<>(Arrays.asList("az","","cb","hi")))).containsExactly("","cb","hi","az");
    }

    @Test
    public void sortStringsByCostShouldDropNullStringsFromSortedList () {
        assertThat(stringSorter.sortStringsByCost(new ArrayList<>(Arrays.asList("az",null,"cb","hi")))).containsExactly("cd","hi","az");
    }

}
