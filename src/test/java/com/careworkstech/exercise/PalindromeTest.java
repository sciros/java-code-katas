package com.careworkstech.exercise;

import static org.assertj.core.api.Assertions.*;

import org.junit.Before;
import org.junit.Test;

public class PalindromeTest {
    private Palindrome palindrome;

    @Before
    public void setUp() {
        palindrome = new Palindrome();
    }

    @Test
    public void isPalindromeShouldReturnTrueGivenPalindromeString() {
        assertThat(palindrome.isPalindrome("racecar")).isTrue();
        assertThat(palindrome.isPalindrome("tacocat")).isTrue();
        assertThat(palindrome.isPalindrome("ABBA")).isTrue();
    }

    @Test
    public void isPalindromeShouldReturnFalseGivenNonPalindromeString() {
        assertThat(palindrome.isPalindrome("racecat")).isFalse();
        assertThat(palindrome.isPalindrome("tacocar")).isFalse();
        assertThat(palindrome.isPalindrome("palindrome")).isFalse();
    }
}
